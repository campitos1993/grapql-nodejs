import { Prisma } from 'prisma-binding' 

const prisma = new Prisma({
    typeDefs: 'src/generated/prisma.graphql',
    endpoint: 'http://localhost:4466'
}) 

export { prisma as default }

// prisma.query, prisma.mutation prisma.subscription, prisma.exists
 

// const createPostForUser = async (authorId, data) => {
//     const userExists = await prisma.exists.User({ id: authorId })

//     if(!userExists){
//         throw new Error('User not Found')
//     } 

//     const post = await prisma.mutation.createPost({
//         data: {
//             ...data,
//             author: {
//                 connect: {
//                     id: authorId
//                 }
//             }
//         }
//     }, `{ id title body published author { id name } }`)

//     const user = await prisma.query.user({
//         where: {
//             id: authorId
//         }
//     }, `{ id name email post { id title published }}`)
//     return user
// }

// // createPostForUser("s", {
// //     title: "Post creado por medio de una funcion",
// //     body: "Contenido asombroso",
// //     published: true
// // }).then((data) => { 
// //     console.log(JSON.stringify(data, undefined, 2))
// // }).catch((err) => {
// //     console.log(err)
// // })


 
// const updatePostForUser = async (postId, data) =>{
//     const postExists = await prisma.exists.Post({id: postId})

//     if(!postExists){
//         throw new Error('Post not found')
//     }
//     const post = await prisma.mutation.updatePost({
//         where: {
//             id: postId
//         },
//         data
//     }, `{ author { id name email post {id title body published } }} `)

    

//     return post.author
// }

// updatePostForUser("ck6gy97wv004x0850d5atev93", {published: false}).then((data) => {
//     console.log(data)

// }).catch((err) => {
//     console.log(err)
// })


// // prisma.query.users(null, `{ 
// //     id 
// //     name  
// //     email 
// //     post { 
// //         id 
// //         title 
// //     }
// // }`).then((data) => {
// //     console.log(JSON.stringify(data, undefined, 2))   

// // }).catch( (err)=> {  
// //    console.log(err); 
// // })   

// // prisma.query.comments(null, `{
// //     id
// //     text
// //     author {
// //         id
// //         name
// //     }
// //     post {
// //         id
// //         title
// //         body
// //         published
// //     }

// // }`).then((data) => {
// //     console.log(JSON.stringify(data, undefined, 2))
// // }).catch((err) => {
// //     console.log(err)
// // })

// // prisma.mutation.createPost({
// //     data: {
// //         title: "My new GraphQL post is live!",
// //         body: "You can find the new course here",
// //         published: true,
// //         author: {
// //             connect: {
// //                 id: "ck6gy8qal004i0850w6dbe2ai"
// //             }
// //         }
// //     } 
// // }, `{ id title body published }`).then((data) => {
// //     console.log(data)
// //     return prisma.query.users(null, `{ id name post { id title } }`)
// // }).then((data) => {
// //     console.log(JSON.stringify(data, undefined, 2))
// // }).catch((err) => {
// //     console.log(err)
// // })

// // prisma.mutation.updatePost({
// //     data: {
// //         title: "Post Modificado con Prisma JS",
// //         body: "Contenido del Post",
// //         published: false
// //     },
// //     where: {
// //         id: "ck6gy97wv004x0850d5atev93"
// //     }
// // }, `{ id title body published author { id name } }`).then((data) => {
// //     console.log(data)
// // }).catch((err) => {
// //     console.log(err)
// // })