import  uuid  from 'uuid'
import db from '../db'



const Mutation = {
    async createUser(parent, args, { prisma }, info){
       const emailTaken = await prisma.exists.User({ email: args.data.email })

       if(emailTaken) {
           throw new Error('Email Taken')
       }

       return prisma.mutation.createUser({ data: args.data }, info)

    },
 
    async updateUser(parent, args, { prisma }, info){
        const userExists = prisma.exists.User({ id: args.id })

        if(!userExists){
            throw new Error('User not Found')
        }

       return prisma.mutation.updateUser({
           where: {
               id: args.id
           },
           data: args.data
       }, info)
    },

    async deleteUser(parent, args, { prisma }, info){
        const userExists = await prisma.exists.User({id: args.id})

        if(!userExists){
            throw new Error('User not Found')
        }

        return prisma.mutation.deleteUser({ where: { id: args.id }}, info)

    },

    async createPost(parent, args, { prisma }, info){
        const userExists = await prisma.exists.User({ id: args.data.author })


        if(!userExists){
            throw new Error('User not Found')
        }

        return prisma.mutation.createPost({ 
                data: {
                    title: args.data.title,
                    body: args.data.body,
                    published: args.data.published,
                    author: {
                        connect: {
                            id: args.data.author
                        }
                    }
                }
            }, info) 
    },

    updatePost(parent, args, { db, pubsub }, info){
        const { id, data } = args
        const post = db.posts.find((post) => post.id === id)
        const originalPost = { ...post }

        if(!post){
            throw new Error('Post not Found')
        }

        if(typeof data.title === 'string'){
            post.title = data.title
        }
        if(typeof data.body === 'string'){
            post.body = data.body
        }
        if(typeof data.published === 'boolean'){
            post.published = data.published

            if(originalPost.published && !post.published){
                //Deleted
                pubsub.publish('post', {
                    post: {
                        mutation: 'DELETED',
                        data: originalPost
                    }
                })
            }
            else if(!originalPost.published && post.published){
                //Created
                pubsub.publish('post', {
                    post: {
                        mutation: 'CREATED',
                        data: post
                    }
                })
            }
        }
        else if(post.published){
            //Updated
            pubsub.publish('post', {
                post: {
                    mutation: 'UPDATED',
                    data: post
                }
            })
        }
        if(typeof data.author === 'string'){
            const user = db.users.find((user) => user.id === data.author)
            if(!user){
                throw new Error('User not Found')
            }
            post.author = data.author
        }

        return post
    },

    deletePost(parent, args, { db, pubsub }, info){
        const postIndex = db.posts.findIndex((post) => post.id === args.id)

        if(postIndex === -1){
            throw new Error('Post not Found')
        }

        const [post] = db.posts.splice(postIndex, 1)

        db.comments = db.comments.filter((comment) => comment.post !== args.id)

        if(post.published){
            pubsub.publish('post', {
                post:{
                    mutation: 'DELETED',
                    data: post
                }
            })
        }

        return post
    },

    createComment(parent, args, { db, pubsub }, info){
        const userExists = db.users.some((user) => user.id === args.comment.author)
        const postExists = db.posts.some((post) => post.id === args.comment.post && post.published)

        if(!userExists || !postExists){
            throw new Error('User or Post not Found')
        }
        

        const comment = {
            id: uuid(),
            ...args.comment
        }

        db.comments.push(comment)
        pubsub.publish(`comment ${comment.post}`, { 
            comment: {
                mutation: 'CREATED',
                data: comment
            }
        })
        return comment
    },

    updateComment(parent, args, { db, pubsub }, info){
        const { id, data } = args
        const comment = db.comments.find((comment) => comment.id === id)

        if(!comment){
            throw new Error('Comment not Found')
        }
        if(typeof data.text === "string"){
            comment.text = data.text
        }
        if(typeof data.author === "string"){
            const user = db.users.find((user) => user.id === data.author)
            if(!user){
                throw new Error('User not found')
            }
            comment.author = data.author
        }
        if(typeof data.post === "string"){
            const post = db.posts.find((post) => post.id === data.post)
            if(!post){
                throw new Error('Post not found')
            }
            comment.post = data.post
        }

        pubsub.publish(`comment ${comment.post}`, {
            comment: {
                mutation: 'UPDATED',
                data: comment
            }
        })

        return comment
    }, 

    deleteComment(parent, args, { db, pubsub }, info){
        const commentIndex = db.comments.findIndex((comment) => comment.id === args.id)

        if(commentIndex === -1){
            throw new Error('Comment not Found')
        }

        const [comment] = db.comments.splice(commentIndex, 1)
        
        pubsub.publish(`comment ${comment.post}`, {
            comment: {
                mutation: 'DELETED',
                data: comment
            }
        })

        return comment
    }
}

export { Mutation as default}