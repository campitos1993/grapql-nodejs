import { db } from '../db'
const Query = {
    users(parent, args, { prisma }, info){
        const opArgs = {}
        if(args.query){
            opArgs.where = {
                OR: [{
                    name_contains: args.query
                }, {
                    email_contains: args.query
                }]
            }
        }
        return prisma.query.users(opArgs, info)
    },
    posts(parent, args, { db, prisma }, info){
        const opArgs = {}

        if(args.query){
            opArgs.where = {
                OR: [
                        {
                            title_contains: args.query
                        },
                        {
                            body_contains: args.query
                        }
                    ]
            }
        }

        return prisma.query.posts(opArgs, info)
        
    },
    comments(parent, args, { prisma }, info){
        return prisma.query.comments(null, info)
    },
    me() {
        return {
            id: '12345',
            name: 'Mike',
            email: 'email@example.com',
            age: 20
        }
    },
    post() {
        return {
            id: '12323',
            title: 'Post Testing',
            body: 'This is the body of the post',
            published: false
        }
    }

}

export { Query as default}