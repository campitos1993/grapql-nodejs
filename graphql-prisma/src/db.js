let users = [
    {
        id: "1",
        name: "Daniel Campos",
        email: "example@correo.com",
        age: 26
    },
    {
        id: "2",
        name: "Eduardo Campos",
        email: "example2@correo.com",
        age: 27
    },
    {
        id: "3",
        name: "Sofia Torres",
        email: "example3@correo.com",
        age: 26
    },
    {
        id: "3",
        name: "Sofia Torres",
        email: "example3@correo.com",
        age: 26
    }
]

let posts = [
    {
        id: "1",
        title: "Post Mutation", 
        body: "Content of the Post",
        published: true,
        author: "1"
    },
    {
        id: "2",
        title: "Post Mutation 2", 
        body: "Content of the Post 2",
        published: true,
        author: "2"
    },
    {
        id: "3",
        title: "Post Mutation 3", 
        body: "Content of the Post 3",
        published: true,
        author: "3"
    }
]

let comments = [
   {
       id: "1",
       text: "Comentario Genial",
       author: "1",
       post: "1"
   },
   {
        id: "2",
        text: "Comentario Genial",
        author: "1",
        post: "1"
    },
    {
        id: "3",
        text: "Comentario Genial",
        author: "3",
        post: "3"
    },
    {
        id: "4",
        text: "Comentario Genial",
        author: "2",
        post: "2"
    }
]

const db = {
    users,
    posts,
    comments
}

export { db as default }